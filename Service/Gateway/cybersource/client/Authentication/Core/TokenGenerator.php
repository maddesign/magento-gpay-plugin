<?php

namespace UnboundCommerce\GooglePay\Service\Gateway\cybersource\client\Authentication\Core;

interface TokenGenerator
{
    public function generateToken($resourcePath, $payloadData, $method, $merchantConfig);
}
